package Com.Web;

import java.util.List;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Com.Model.Students;
import Com.dao.EmployeeDAO;


@WebServlet("/GetAllEmployees")
public class GetAllEmployees extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		EmployeeDAO empDAO = new EmployeeDAO();
		List<Students> empList = empDAO.getAllEmployees();
		
		RequestDispatcher rd = request.getRequestDispatcher("HRHomePage.jsp");
		rd.include(request, response);
		
		
		if (empList != null) {
			out.print("<center>");
			
			out.print("<table border=2>");
			out.print("<thead>");
			out.print("<tr>");
			out.print("<th>EmpId</th>");
			out.print("<th>EmpName</th>");
			out.print("<th>Salary</th>");
			out.print("<th>Gender</th>");
			out.print("<th>EmailId</th>");
			out.print("</tr>");
			out.print("</thead>");
			
			for (Students emp : empList) {
				out.print("<tr>");
				out.print("<td>" + emp.getEmpId()   + "</td>");
				out.print("<td>" + emp.getEmpName() + "</td>");
				out.print("<td>" + emp.getSalary()  + "</td>");
				out.print("<td>" + emp.getGender()  + "</td>");
				out.print("<td>" + emp.getEmailId() + "</td>");
				out.print("</tr>");
			}
			
			out.print("</table>");
			
			out.print("</center>");	
		} else {
			out.print("<center><h1 style='color:red;'>Record(s) Not Found!!!</h1></center>");
		}
		
	
	
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doGet(request, response);
	}

}
