package Com.Web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Com.dao.EmployeeDAO;


@WebServlet("/Register")
public class Register extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		String empName = request.getParameter("empName");
		String emailId  = request.getParameter("emailId");
		String gender = request.getParameter("gender");
		Double salary =  Double.parseDouble(request.getParameter("salary"))  ;
		String password = request.getParameter("password");
        EmployeeDAO empDAO  = new EmployeeDAO();
		
		int result  = empDAO.empRegister(empName,salary,gender,emailId,password);
		
		if(result == 0){
			out.print("<body>");
			out.print("<h2>Registeration Failed </h2>");
			out.print("</body>");
			
			RequestDispatcher rd = request.getRequestDispatcher("Registration.html");
			rd.include(request, response);

		}
		else{
			
			RequestDispatcher rd = request.getRequestDispatcher("Login.html");
			rd.forward(request, response);
		}
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
