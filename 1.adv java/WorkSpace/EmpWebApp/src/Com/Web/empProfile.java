package Com.Web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Com.Model.Students;

@WebServlet("/empProfile")
public class empProfile extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		HttpSession session = request.getSession(false);
		Students emp=(Students)session.getAttribute("emp");
		RequestDispatcher rd = request.getRequestDispatcher("EmpHomePage.jsp");
		rd.include(request, response);
		
		out.print("<br/>");
		out.print("<center>");
		
		out.print("<table border=2>");
		out.print("<thead>");
		out.print("<tr>");
		out.print("<th>EmpId</th>");
		out.print("<th>EmpName</th>");
		out.print("<th>Salary</th>");
		out.print("<th>Gender</th>");
		out.print("<th>EmailId</th>");
		out.print("<th>Password</th>");
		out.print("</tr>");
        out.print("</thead>");
		
		out.print("<tr>");
		out.print("<td>" + emp.getEmpId()   + "</td>");
		out.print("<td>" + emp.getEmpName() + "</td>");
		out.print("<td>" + emp.getSalary()  + "</td>");
		out.print("<td>" + emp.getGender()  + "</td>");
		out.print("<td>" + emp.getEmailId() + "</td>");
		out.print("<td>" + emp.getPassword() + "</td>");
		out.print("</tr>");
		
		out.print("</table>");			
		out.print("</center>");
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doGet(request, response);
	}

}
