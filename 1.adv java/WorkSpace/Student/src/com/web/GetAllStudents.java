package com.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.StudentDAO;
import com.model.Student;



@WebServlet("/GetAllStudents")
public class GetAllStudents extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		StudentDAO stdDAO = new StudentDAO();
		List<Student> stdList = stdDAO.getAllStudent();
		
		RequestDispatcher rd = request.getRequestDispatcher("HodHomePage");
		rd.include(request, response);
		
		
		if (stdList != null) {
			out.print("<center>");
			
			out.print("<table border=2>");
			out.print("<thead>");
			out.print("<tr>");
			out.print("<th>stdId</th>");
			out.print("<th>stdName</th>");
			out.print("<th>Gender</th>");
			out.print("<th>EmailId</th>");
			out.print("</tr>");
			out.print("</thead>");
			
			for (Student std : stdList) {
				out.print("<tr>");
				out.print("<td>" + std.getSTDId()   + "</td>");
				out.print("<td>" + std.getSTDName() + "</td>");
				out.print("<td>" + std.getGender()  + "</td>");
				out.print("<td>" + std.getEmailId() + "</td>");
				out.print("</tr>");
			}
			
			out.print("</table>");
			
			out.print("</center>");	
		} else {
			out.print("<center><h1 style='color:red;'>Record(s) Not Found!!!</h1></center>");
		}
		
	
	
	
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
