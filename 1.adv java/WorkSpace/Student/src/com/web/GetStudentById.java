package com.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.StudentDAO;
import com.model.Student;


@WebServlet("/GetStudentById")
public class GetStudentById extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		int stdId = Integer.parseInt(request.getParameter("stdId"));
		
		StudentDAO stdDAO = new StudentDAO();
		Student std = stdDAO.getStudentById(stdId);
		
		RequestDispatcher rd = request.getRequestDispatcher("HodHomePage");
		rd.include(request, response);
		
		if (std != null) {
			out.print("<br/>");
			out.print("<center>");
			
			out.print("<table border=2>");
			out.print("<thead>");
			out.print("<tr>");
			out.print("<th>StdId</th>");
			out.print("<th>StdName</th>");
			out.print("<th>Gender</th>");
			out.print("<th>EmailId</th>");
			out.print("<th>Password</th>");
			out.print("</tr>");
			out.print("</thead>");
			
			out.print("<tr>");
			out.print("<td>" + std.getSTDId()   + "</td>");
			out.print("<td>" + std.getSTDName() + "</td>");
			out.print("<td>" + std.getGender()  + "</td>");
			out.print("<td>" + std.getEmailId() + "</td>");
			out.print("<td>" + std.getPassword() + "</td>");
			out.print("</tr>");
			
			out.print("</table>");			
			out.print("</center>");	
		} else {
			out.print("<center>");	
			out.print("<br/><h3 style='color:red;'>Students Record Not Found!!!</h3>");
			out.print("</center>");	
		}
	
	
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doGet(request, response);
	}

}
