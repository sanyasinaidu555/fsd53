package com.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.dao.StudentDAO;




@WebServlet("/Register")
public class Register extends HttpServlet {

    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		String stdName = request.getParameter("stdName");
		String emailId  = request.getParameter("emailId");
		String gender = request.getParameter("gender");
		String password = request.getParameter("password");
        StudentDAO stdDAO  = new StudentDAO();
		
		int result  = stdDAO.stdRegister(stdName,gender,emailId,password);
		
		if(result == 0){
			out.print("<body>");
			out.print("<h2>Registeration Failed </h2>");
			out.print("</body>");
			
			RequestDispatcher rd = request.getRequestDispatcher("Registration.html");
			rd.include(request, response);

		}
		else{
			
			RequestDispatcher rd = request.getRequestDispatcher("login.html");
			rd.forward(request, response);
		}
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		doGet(request, response);
	}

}
