package com.web;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.model.Student;



@WebServlet("/StudentProfile")
public class StudentProfile extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		HttpSession session = request.getSession(false);
		Student std=(Student)session.getAttribute("std");
		RequestDispatcher rd = request.getRequestDispatcher("StudentHomePage");
		rd.include(request, response);
		
		out.print("<br/>");
		out.print("<center>");
		
		out.print("<table border=2>");
		out.print("<thead>");
		out.print("<tr>");
		out.print("<th>stdId</th>");
		out.print("<th>stdName</th>");
		out.print("<th>Gender</th>");
		out.print("<th>EmailId</th>");
		out.print("<th>Password</th>");
		out.print("</tr>");
        out.print("</thead>");
		
		out.print("<tr>");
		out.print("<td>" + std.getSTDId()   + "</td>");
		out.print("<td>" + std.getSTDName() + "</td>");
		out.print("<td>" + std.getGender()  + "</td>");
		out.print("<td>" + std.getEmailId() + "</td>");
		out.print("<td>" + std.getPassword() + "</td>");
		out.print("</tr>");
		
		out.print("</table>");			
		out.print("</center>");
	
	
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doGet(request, response);
	}

}
