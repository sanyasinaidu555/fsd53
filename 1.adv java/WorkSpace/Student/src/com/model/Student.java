package com.model;

public class Student {
	private int stdId;
	private String stdName;
	private String gender;
	private String emailId;
	private String password;
	
	public Student () {
		super();
	}

	public Student (int stdId, String stdName, String gender, String emailId, String password) {
		super();
		this.stdId = stdId;
		this.stdName = stdName;
		this.gender = gender;
		this.emailId = emailId;
		this.password = password;
	}

	public int getSTDId() {
		return stdId;
	}
	public void setSTDId(int empId) {
		this.stdId = empId;
	}

	public String getSTDName() {
		return stdName;
	}
	public void setSTDName(String empName) {
		this.stdName = empName;
	}

	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "student [stdId=" + stdId + ", stdName=" + stdName + ", gender=" + gender
				+ ", emailId=" + emailId + ", password=" + password + "]";
	}
	
	
}
