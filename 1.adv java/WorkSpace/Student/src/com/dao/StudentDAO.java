package com.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.db.DBConnection;
import com.model.Student;



public class StudentDAO {
public Student stdLogin(String emailId, String password) {
		
		Connection con = DBConnection.getConnection();
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		if (con == null) {
			System.out.println("Cannot Establish the Connection to the Database...");
			return null;
		}
				
		try {
			String selectQry = "Select * from student where emailId = ? and password = ?";
			
			pst = con.prepareStatement(selectQry);
			pst.setString(1, emailId);
			pst.setString(2, password);
			rs = pst.executeQuery();
			
			if (rs.next()) {
				
				Student std = new Student();
				
				std.setSTDId(rs.getInt("stdId"));
				std.setSTDName(rs.getString(2));
				std.setGender(rs.getString(3));
				std.setEmailId(rs.getString(4));
				std.setPassword(rs.getString(5));
				
				return std;
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			if (con != null) {
				try {
					rs.close();
					pst.close();
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
				
		return null;		
	}


public List<Student> getAllStudent() {
	Connection con = DBConnection.getConnection();
	PreparedStatement pst = null;
	ResultSet rs = null;
	
	if (con == null) {
		System.out.println("Cannot Establish the Connection to the Database...");
		return null;
	}
			
	try {
		String selectQry = "Select * from student";
		
		pst = con.prepareStatement(selectQry);
		rs = pst.executeQuery();
		
		List<Student> stdList = new ArrayList<Student>();
		
		while (rs.next()) {
			
			Student std = new Student();
			
			std.setSTDId(rs.getInt("stdId"));
			std.setSTDName(rs.getString(2));
			std.setGender(rs.getString(3));
			std.setEmailId(rs.getString(4));
			std.setPassword(rs.getString(5));
			
			stdList.add(std);
		}
		
		return stdList;
		
	} catch (SQLException e) {
		e.printStackTrace();
	}
	
	finally {
		if (con != null) {
			try {
				rs.close();
				pst.close();
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
			
	return null;		
}
public int stdRegister(String stdName,String gender,String emailId,String password) {
	
	Connection con = DBConnection.getConnection();
	PreparedStatement pst = null;
    int result = 0;
    
    if(con ==null){
    	System.out.println("Cannot establish connection with the database...");
    }
    
    try {
    	
	    String query = "insert into student(stdName,gender,emailId,password) values(?,?,?,?)";
		pst  = con.prepareStatement(query);
		pst.setString(1,stdName);
		pst.setString(2,gender);
		pst.setString(3,emailId);
		pst.setString(4,password);
		
		result = pst.executeUpdate();
		
		return result;
		
	} catch (SQLException e) {
		e.printStackTrace();
	}
    finally{
    	
    	if(con != null){
    		try {
				pst.close();
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
    	}
    }
    
	
	return 0;
}

public Student getStudentById(int empId) {
	Connection con = DBConnection.getConnection();
	PreparedStatement pst = null;
	ResultSet rs = null;
	
	if (con == null) {
		System.out.println("Cannot Establish the Connection to the Database...");
		return null;
	}
			
	try {
		String qry = "Select * from student where stdId = ?";
		
		pst = con.prepareStatement(qry);
		pst.setInt(1, empId);
		rs = pst.executeQuery();
		
		if (rs.next()) {
			
			Student std = new Student();
			
			std.setSTDId(rs.getInt(1));
			std.setSTDName(rs.getString(2));
			std.setGender(rs.getString(3));
			std.setEmailId(rs.getString(4));
			std.setPassword(rs.getString(5));
			
			return std;
		}
		
	} catch (SQLException e) {
		e.printStackTrace();
	}
	
	finally {
		if (con != null) {
			try {
				rs.close();
				pst.close();
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
			
	return null;		
}
}


